import Vuex from 'vuex'
import axios from 'axios'
import Cookie from 'js-cookie'

const createStore = () => {
    return new Vuex.Store({
        state: {
            Kategoriler: [],
            adminkey: null
        },
        mutations: {
            setAdminkey(state, adminkey) {
                // Cookie.set('adminkey', adminkey)
                 state.adminkey = adminkey
             },
             clearAdminkey(state) {
                 Cookie.remove('adminkey')
                 localStorage.removeItem('adminkey')
                 state.adminkey = null
             },
            Setkategoriler(state, kategoriler) {
                state.Kategoriler = kategoriler
            }
        },
        actions: {
            nuxtServerInit(vuexContext, context) {
                 return  axios.get('http://localhost:3000/api/kategoriler')
                .then((response) => {
                    vuexContext.commit('Setkategoriler', response.data.kategoriler)
                })
            },
            initAdminkey(vuexContext, req) {
                let token;
                if (req) {
                    //server üzerinde calışıyoruz
                    if (!req.headers.cookie) {
                        return
                    }

                    token = req.headers.cookie.split(';').find(c => c.trim().startsWith('adminkey='))
                    if (token) {
                        token = token.split('=')[1]
                            console.log('token')
                    }

                } else {
                    //client üzerinden çalışıyoruz
                     token = localStorage.getItem('adminkey')
                    if(!token){
                        console.log('token')
                        return;
                    }

                }

                vuexContext.commit('setAdminkey', token)
            },
            login(vuexContext, adminkey)  {
                Cookie.set('adminkey' , adminkey)
                localStorage.setItem('adminkey', adminkey)
                vuexContext.commit('setAdminkey', adminkey)
            },
            cikisyap(vuexContext){
                 vuexContext.commit('clearAdminkey')  
            }
        },
        getters: {
            getKat(state) {
                return state.Kategoriler
            },
            isAdminGiris(state) {
                return state.adminkey != null
            }
        }
    })
}
export default createStore