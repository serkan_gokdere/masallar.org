const express = require('express')
const mysql = require('mysql')
var bodyParser = require('body-parser')
var dashify = require('dashify');
var multer =require('multer')
const cors = require("cors");




const app = express()
app.use(cors())

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'static/uploads/')
    console.log(null)
  },
  filename: (req, file, cb) => {
    var datetimestamp = Date.now();
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]);
  }
});
var upload = multer({storage: storage,
  dest: './uploads',});


const PORT = '5000' || process.env.PORT;
app.post('/upload', upload.single('file'), (req, res) => {
  res.json({ file: req.file });
});

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));










let connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'kitap_ozetleri_db'
})

app.use(bodyParser());
connection.connect(function (err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }

  console.log('connected as id ' + connection.threadId);
});

//Masal ve Hikaye Ekle
app.post('/MasalYaz', (req, res) => {        

  let title = req.body.title;
  let yazar = req.body.yazar;
  let date ='now()';
  let kategori_id = req.body.kategori_id;
  let slug = dashify(req.body.title);
  let text = req.body.text;
  let selectedFiles = req.body.selectedFiles
  //let selectedFile = req.body.selectedFile


  let query = connection.query('INSERT INTO Masal SET ?', {  title: title,yazar:yazar,date:date,kategori_id:kategori_id,slug:slug,text:text,selectedFiles:selectedFiles}, function (error, results, fields) {

    // Neat!
  });
  console.log(query.sql);
}),


//Kategori Ekle
app.post('/Kategoriekle', (req, res) => {
  let kategoriadi = req.body.kategoriadi
  let kategoridesc = req.body.kategoridesc
  let slug = dashify(req.body.kategoriadi);
  //let selectedFile = req.body.selectedFile
  let query = connection.query('INSERT INTO kategori SET ?', { kategoriadi:kategoriadi,kategoridesc:kategoridesc, slug:slug}, function (error, results, fields) {

    // Neat!
  });
  console.log(query.sql);
}),

//Kategori Sil
app.get('/kategorisil/:id', (req, res) => {
  let id = req.params.id;
  connection.query("DELETE FROM kategori WHERE id= '" + id + "'", function (error, results, fields) {
    if (error) throw error;
    console.log('deleted ' + results.affectedRows + ' rows');
    
  })
}),
//Kategori Düzenle
app.get('/kategoriduzenle/:id', (req, res) => {
  let id = req.params.id;
  connection.query("UPDATE kategori SET id= '" + id + "'" , function (error, results, fields) {
    if (error) throw error;
    console.log('changed ' + results.changedRows + ' rows');
  })
}),
//Yorum ekle
app.post('/yorumEkle', (req, res) => {
  let isim = req.body.isim
  let soyad = req.body.soyad
  let yorum = req.body.yorum
  let masal_id = req.body.masal_id
  //let selectedFile = req.body.selectedFile
  let query = connection.query('INSERT INTO yorumlar SET ?', { isim: isim , soyad:soyad, yorum:yorum, masal_id:masal_id} , function (error, results, fields) {

    // Neat!
  });
  console.log(query.sql);
}),

//Yorumları Cek 
app.get('/yorumlaricek/:masalId', (req, res) => {
  let masalId = req.params.masalId;

connection.query("select y.`isim`, y.`soyad`, y.`yorum`, y.`id` , m.`yorumId` from yorumlar y inner join Masal m on y.`masal_id`=m.`id` where m.slug='" + masalId + "'  ORDER BY id DESC LIMIT 5", (err, results, fields) => {
  
    res.status(200).json({
      yorumlar: results
    });

  });
}),

//Admin yorumları Göster
app.get('/adminyorumlar', (req, res) => {
  connection.query("select a.`yorum`, a.`id`, m.`title`  from yorumlar a inner join Masal m on a.`masal_id`= m.`id`" , (err, results, fields) => {
    res.status(200).json({
      adminyorumlar: results
    });

  });
}),

//Rasgele veri çekme slider
app.get('/slider', (req, res) => {
  connection.query("select * from Masal order by rand() limit 1", (err, results, fields) => {
    res.status(200).json({
      slider: results
    });

  });
}),

//Kategori Desc Çekme

app.get('/katdesc/:katdesc', (req, res) => {
  let katdesc = req.params.katdesc
  connection.query("SELECT * from kategori where slug= '" + katdesc + "'", (err, results, fields) => {
    res.status(200).json({
      katdesc: results
    });

  });
}),

//Kategori Çekme
app.get('/kategoriler', (req, res) => {
  connection.query("SELECT * from kategori", (err, results, fields) => {
    res.status(200).json({
      kategoriler: results
    });

  });
}),



//Masallar ve Hiakeyeler  Çekme
app.get('/', (req, res) => {
  connection.query("SELECT * from Masal ORDER BY id DESC LIMIT 12", (err, results, fields) => {
    res.status(200).json({
      Masal: results
    });

  });
})

//Son 4 Masal 
app.get('/sonmasal', (req, res) => {
  connection.query("SELECT * from Masal   ORDER BY id DESC LIMIT 4", (err, results, fields) => {
    res.status(200).json({
      sonmasal: results
    });

  });
})



//Kategorileri Çekme



  //Masal ve Hikaye Detay Çekme 

  app.get('/masal/:masalID', (req, res) => {
    let masalID = req.params.masalID;
    connection.query("SELECT * from Masal where slug='" + masalID + "' ", (err, results, fields) => {
      res.status(200).json({
        masaldetay: results
      });

    })
  }),

  //Masalları Kategorilere Göre Çekme
  app.get('/kategori/:kategoriadi', (req, res) => {
    let kategoriadi = req.params.kategoriadi;

connection.query("select  k.`kategoridesc`, k.`kategoriadi`,m.`title`,m.`text`,m.`slug`,m.`date`, m.`selectedFiles` from  kategori k inner join Masal m on k.id=m.kategori_id where k.slug='" + kategoriadi + "'  ", (err, results, fields) => {

      res.status(200).json({
        katmasallar: results
      });

    });
  }),





  module.exports = {
    path: '/api',
    handler: app
  }